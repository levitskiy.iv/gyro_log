#![no_std]
#![no_main]
#![feature(alloc_error_handler)]
mod sdcard;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();
// 128K heap, leaving 128K for stack (which is overkill)
const HEAP_SIZE: usize = 1024 * 16;

// define what happens in an Out Of Memory (OOM) condition
#[alloc_error_handler]
fn alloc_error(_layout: Layout) -> ! {
    defmt::println!("OOM!");
    asm::bkpt();
    loop {}
}


use core::alloc::Layout;
use alloc_cortex_m::CortexMHeap;
use heapless::spsc::Queue;
use cortex_m::asm;
use defmt_rtt as _;
use panic_probe as _;
use fatfs::{
    DefaultTimeProvider, Dir, File, FileSystem, FsOptions, IoBase, LossyOemCpConverter,
    OemCpConverter, Read, Seek, SeekFrom, TimeProvider, Write,
};
use mpu9250::UnscaledImuMeasurements;
use stm32f4xx_hal::timer::SysDelay;

// Create a buffer with 1024 elements
static mut IMU_QUE: Queue<UnscaledImuMeasurements<[i16; 3]>, 5000> = Queue::new();

#[defmt::panic_handler]
fn panic() -> ! {
    cortex_m::asm::udf()
}

#[rtic::app(device = stm32f4xx_hal::pac)]
mod app {
    use fatfs::{Write, DateTime, DefaultTimeProvider, File, LossyOemCpConverter};
    use stm32f4xx_hal::prelude::*;
    use core::fmt::Write as fmtwrite;
    use core::time::Duration;
    use cortex_m::asm;
    use cortex_m::asm::delay;
    use defmt::println;
    use mpu9250::{AccelDataRate, Dlpf, GyroTempDataRate, Imu, ImuMeasurements, Mpu9250, MpuConfig, SpiDevice, UnscaledImuMeasurements};
    use stm32f4xx_hal::gpio::{Alternate, Edge, Input, Output, PB0};
    use stm32f4xx_hal::sdio::{SdCard, Sdio};
    use stm32f4xx_hal::spi::{Mode, Phase, Polarity, Spi};
    use crate::{ALLOCATOR, HEAP_SIZE, IMU_QUE, panic, sdcard};
    use crate::sdcard::open;
    use heapless::String;
    use stm32f4xx_hal::gpio::Speed::High;


    #[shared]
    struct Shared {}

    #[local]
    struct Local {
        log_fs: fatfs::FileSystem< sdcard::Card, fatfs::DefaultTimeProvider, LossyOemCpConverter>,
        imu_int: PB0<Input>,
        imu: Mpu9250<SpiDevice<stm32f4xx_hal::spi::Spi<stm32f4xx_hal::pac::SPI1, (stm32f4xx_hal::gpio::Pin<'A', 5_u8, Alternate<5_u8>>, stm32f4xx_hal::gpio::Pin<'B', 4_u8, Alternate<5_u8>>, stm32f4xx_hal::gpio::Pin<'A', 7_u8, Alternate<5_u8>>)>, stm32f4xx_hal::gpio::Pin<'A', 4_u8, Output>>, Imu>,
    }

    #[init]
    fn init(ctx: init::Context) -> (Shared, Local, init::Monotonics) {
        let mut dp = ctx.device;
        let cp = ctx.core;

        unsafe { ALLOCATOR.init(cortex_m_rt::heap_start() as usize, HEAP_SIZE) }

        let rcc = dp.RCC.constrain();
        let clocks = rcc.cfgr
            .use_hse(24.MHz())
            .require_pll48clk()
            .sysclk(100.MHz())
            .pclk1(50.MHz())
            .pclk2(100.MHz())
            .freeze();

        let mut syscfg = dp.SYSCFG.constrain();

        let mut delay = cp.SYST.delay(&clocks);

        let gpioa = dp.GPIOA.split();
        let gpiob = dp.GPIOB.split();

        //imu pins
        let ncs_pin = gpioa.pa4.into_push_pull_output();
        let spi1_sck = gpioa.pa5.into_alternate();
        let spi1_mosi = gpioa.pa7.into_alternate();
        let mut imu_int = gpiob.pb0.into_input();
        let spi1_miso = gpiob.pb4.into_alternate();

        //sdcard gpio
        let mut sd_d0 = gpiob.pb7.into_alternate().internal_pull_up(true);
        let mut sd_d1 = gpioa.pa8.into_alternate().internal_pull_up(true);
        let mut sd_d2 = gpioa.pa9.into_alternate().internal_pull_up(true);
        let mut sd_d3 = gpiob.pb5.into_alternate().internal_pull_up(true);
        let mut sd_clk = gpiob.pb15.into_alternate().internal_pull_up(true);
        let mut sd_cmd = gpioa.pa6.into_alternate().internal_pull_up(true);
        sd_d0.set_speed(High);
        sd_d1.set_speed(High);
        sd_d2.set_speed(High);
        sd_d3.set_speed(High);
        sd_clk.set_speed(High);
        sd_cmd.set_speed(High);
        let mut sdio: Sdio<SdCard> = Sdio::new(dp.SDIO, (sd_clk, sd_cmd, sd_d0, sd_d1, sd_d2, sd_d3), &clocks);

        sdcard::init_sdio(&mut sdio, &mut delay);

        let mut block = [0u8; 512];

        let res = sdio.read_block(0, &mut block);
        defmt::println!("sdcard read result {:?}", defmt::Debug2Format(&res));

        let mut log_fs = open(sdio).unwrap();

        //let root_dir = log_fs.root_dir();

        //defmt::println!("opened root");

        //let mut log_file = log_fs.root_dir().create_file("file2.gcsv").unwrap();
        //log_file.write_all(b"Hello World!").unwrap();

        imu_int.make_interrupt_source(&mut syscfg);
        imu_int.enable_interrupt(&mut dp.EXTI);
        imu_int.trigger_on_edge(&mut dp.EXTI, Edge::Falling);


        let spi1_mode = Mode {
            polarity: Polarity::IdleLow,
            phase: Phase::CaptureOnFirstTransition,
        };

        let spi_mpu = Spi::new(dp.SPI1, (spi1_sck, spi1_miso, spi1_mosi), spi1_mode, 3.MHz(), &clocks);

        // 8Hz
        let gyro_rate = mpu9250::GyroTempDataRate::DlpfConf(mpu9250::Dlpf::_2);

        let mut imu;
        let res = Mpu9250::imu_with_reinit(
            spi_mpu,
            ncs_pin,
            &mut delay,
            &mut MpuConfig::imu()
                .gyro_temp_data_rate(gyro_rate)
                .sample_rate_divisor(3),
            |spi_mpu, ncs_pin| {
                let (dev_spi, (scl, miso, mosi)) = spi_mpu.release();
                let new_spi = dev_spi.spi(
                    (scl, miso, mosi),
                    mpu9250::MODE,
                    20.MHz(),
                    &clocks,
                );
                Some((new_spi, ncs_pin))
            },
        );
        match res {
            Ok(i) => { imu = i},
            Err(e) => {defmt::println!("sdcard read result {:?}", defmt::Debug2Format(&e));
            panic();},
        }
        let mut mpu_conf = mpu9250::MpuConfig::imu();
        mpu_conf.accel_data_rate(AccelDataRate::DlpfConf(Dlpf::_6));
        mpu_conf.gyro_temp_data_rate(GyroTempDataRate::DlpfConf(Dlpf::_6));
        imu.config(&mut mpu_conf);

        let all : UnscaledImuMeasurements<[i16; 3]> = imu.unscaled_all().unwrap();

        let res = imu.enable_interrupts(mpu9250::InterruptEnable::RAW_RDY_EN);
        match res {
            Ok(i) => { defmt::println!("int init ok");},
            Err(e) => {defmt::println!("int init err {:?}", defmt::Debug2Format(&e));
                panic();},
        }
        defmt::println!("Gyro: {:?}", defmt::Debug2Format(&imu.get_enabled_interrupts().unwrap()));

        (Shared {}, Local { log_fs, imu_int, imu }, init::Monotonics())
    }

    #[idle(local=[log_fs])]
    fn idle(ctx: idle::Context) -> ! {
        defmt::println!("idle start!");
        let root_dir = ctx.local.log_fs.root_dir();
        let mut sample_id: u32 = 0;//or better to rename to time?
        let mut buf_str: String<256> = String::new();
        let mut cons = unsafe {IMU_QUE.split().1};
        let time_scale = 0.001;
        let gyro_scale = 0.00122173047;
        let accel_scale = 0.00048828125;
//
        let mut log_file = root_dir.create_file("file_two.gcsv").unwrap();
        defmt::println!("File created!");
//
        log_file.write_all(b"GYROFLOW IMU LOG\n");
        log_file.write_all(b"version,1.1\n");
        log_file.write_all(b"id,gyro_log_\n");
        log_file.write_all(b"orientation,YxZ\n");
        write!(&mut buf_str, "tscale,{time_scale}\n").unwrap();
        write!(&mut buf_str, "gscale,{gyro_scale}\n").unwrap();
        write!(&mut buf_str, "ascale,{accel_scale}\n").unwrap();
        log_file.write_all(buf_str.as_bytes());
        log_file.write_all(b"t,gx,gy,gz,ax,ay,az\n");
        log_file.flush();
        buf_str.clear();
        defmt::println!("File written");
//
        loop {
            match cons.dequeue() {
                Some(data) => {
                    write!(&mut buf_str, "{sample_id},{},{},{},", data.gyro[0], data.gyro[1], data.gyro[2]).unwrap();
                    write!(&mut buf_str, "{},{},{}\n", data.accel[0], data.accel[1], data.accel[2]).unwrap();
                    sample_id = sample_id + 1;
                    log_file.write_all(buf_str.as_bytes());
                    if (sample_id % 4) == 0 {
                        log_file.flush();
                        buf_str.clear();
                    }
                },
                None => { /* sleep */
                //defmt::println!("Sleeping");
                }
            }
    //        //write!(&mut buf, "The temp is: {a}C").unwrap();
    //        //let res = log_file.write_all(buf.as_bytes());
        }
    }

    #[task(binds=EXTI0, local=[imu_int, imu])]
    fn mpu_interrupt(ctx: mpu_interrupt::Context) {
        ctx.local.imu_int.clear_interrupt_pending_bit();
        let mut prod = unsafe {IMU_QUE.split().0};
        //defmt::println!("Added to Que!");
        let res : Result<UnscaledImuMeasurements<[i16; 3]>, _> = ctx.local.imu.unscaled_all();
        match res {
            Ok( all) => {
                unsafe {if IMU_QUE.is_full() {
                    defmt::println!("Que is full!");
                }
                else { prod.enqueue(all).ok().unwrap(); }
                }
            }
            Err(..) => {defmt::println!("Int errr");},
        }
        let mut prod = unsafe {IMU_QUE.split().0};
        //defmt::println!("Gyro: {:?}", all.gyro);
        //defmt::println!("Accel: {:?}", all.accel);
    }

}