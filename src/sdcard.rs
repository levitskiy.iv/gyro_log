use core::cmp::min;
use fatfs::{
    DefaultTimeProvider, Dir, File, FileSystem, FsOptions, IoBase, LossyOemCpConverter,
    OemCpConverter, Read, Seek, SeekFrom, TimeProvider, Write,
};

use stm32f4xx_hal::prelude::*;
use stm32f4xx_hal::sdio::{ClockFreq, SdCard, Sdio};
use stm32f4xx_hal::timer::SysDelay;

use defmt::{info, error, debug, println};

const BLOCK_SIZE: usize = 512;

pub struct Card {
    pos: u64,
    sdio: Sdio<SdCard>,
    num_blocks: u32,
    buf: [u8; BLOCK_SIZE],
    buf_block_index: u32,
    is_dirty: bool,
}

impl Card {
    fn read_block(&mut self, n: u32) -> Result<(), <Self as IoBase>::Error> {
        if n == self.buf_block_index {
            // already have it in cache
            return Ok(());
        }
        // write out the current block if dirty
        self.write_block()?;

        self.do_read_block(n)
    }

    fn do_read_block(&mut self, n: u32) -> Result<(), <Self as IoBase>::Error> {
        self.sdio.read_block(n, &mut self.buf).map_err(|e| {
            error!("error {:?}", defmt::Debug2Format(&e));
            ()
        })?;
        self.buf_block_index = n;
        Ok(())
    }

    fn write_block(&mut self) -> Result<(), <Self as IoBase>::Error> {
        if self.is_dirty {
            // rprintln!("write block {}", self.buf_block_index);
            // rprint!(".");
            self.sdio.write_block(self.buf_block_index, &self.buf).map_err(|e| {
                error!("error {:?}", defmt::Debug2Format(&e));
                ()
            })?;
            self.is_dirty = false;
        }
        Ok(())
    }
}

impl IoBase for Card {
    type Error = ();
}

impl Read for Card {
    fn read(&mut self, mut buf: &mut [u8]) -> Result<usize, Self::Error> {
        //println!("read {} @ {}", buf.len(), self.pos);
        let mut count = 0;

        //self.read_block(0)?;
       // let partition_info = &self.buf[446..][0..16];

        let lba_start = 2048;
        //u32::from_le_bytes([
        //    partition_info[8],
        //    partition_info[9],
        //    partition_info[10],
        //    partition_info[11],
        //]);

        while !buf.is_empty() {
            let n = (lba_start + (self.pos / BLOCK_SIZE as u64) as u32);
            if n >= self.num_blocks {
                // end of disk
                break;
            }
            // fill the buffer
            if let Err(e) = self.read_block(n) {
                error!("error {:?} count {}", defmt::Debug2Format(&e), count);
                // If we didn't manage to read anything, return the error,
                // otherwise return how many bytes we wrote
                if count == 0 {
                    return Err(e);
                } else {
                    return Ok(count);
                }
            }
            let offset = (self.pos % BLOCK_SIZE as u64) as usize;
            // read to the end of the buffer, or the sector, whichever is closer
            let len = min(buf.len(), (BLOCK_SIZE - offset) as usize);
            // rprintln!("offset {} len {} block {}", offset, len, self.buf_block);
            buf[0..len].copy_from_slice(&self.buf[offset..offset + len]);
            buf = &mut buf[len..];
            self.pos += len as u64;
            count += len;
        }
        Ok(count)
    }
}

impl Write for Card {
    fn write(&mut self, mut buf: &[u8]) -> Result<usize, <Self as IoBase>::Error> {
        let mut count = 0;

        //self.read_block(0)?;
        //let partition_info = &self.buf[446..][0..16];

        let lba_start = 2048;
        //    u32::from_le_bytes([
        //    partition_info[8],
        //    partition_info[9],
        //    partition_info[10],
        //    partition_info[11],
        //]);

        while !buf.is_empty() {
            let n = lba_start + (self.pos / BLOCK_SIZE as u64) as u32;
            if n >= self.num_blocks {
                // end of disk
                break;
            }

            // potentially write out a dirty buffer, then fill buffer
            if let Err(e) = self.read_block(n) {
                // If we didn't manage to write anything, return the error,
                // otherwise return how many bytes we wrote
                if count == 0 {
                    return Err(e);
                } else {
                    return Ok(count);
                }
            }
            let offset = self.pos as usize % BLOCK_SIZE;
            // write to the end of the buffer, or the sector, whichever is closer
            let len = min(buf.len(), BLOCK_SIZE - offset);
            // rprintln!("len {}", len);
            self.buf[offset..offset + len].copy_from_slice(&buf[0..len]);
            self.is_dirty = true;
            buf = &buf[len..];
            self.pos += len as u64;
            count += len;
        }
        Ok(count)
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        // rprintln!("flush");
        self.write_block()
    }
}

impl Seek for Card {
    fn seek(&mut self, pos: SeekFrom) -> Result<u64, Self::Error> {
        let new_pos = match pos {
            SeekFrom::Start(p) => Ok(p),
            SeekFrom::End(p) => {
                Ok(((self.num_blocks as usize * BLOCK_SIZE) as i64 + p) as u64)
            }
            SeekFrom::Current(p) => {
                Ok((self.pos as i64 + p) as u64)
            }
        }?;
        self.pos = new_pos;
        Ok(self.pos)
    }
}

type FS = FileSystem<Card, DefaultTimeProvider, LossyOemCpConverter>;

pub fn open(sdio: Sdio<SdCard>) -> Result<FS, fatfs::Error<()>> {
    let blocks = sdio.card().map(|c| c.block_count()).unwrap();
    let mut card = Card {
        sdio,
        pos: 0,
        num_blocks: blocks,
        buf: [0u8; BLOCK_SIZE],
        buf_block_index: 0,
        is_dirty: false,
    };

    // prefetch the first block, making the buffer a valid copy of what's on disk

    card.do_read_block(0)?;

    let fs = FileSystem::new(card, FsOptions::new())?;

    Ok(fs)
}


pub fn init_sdio(sdio: &mut Sdio<SdCard>, delay: &mut SysDelay) {
    info!("detecting sdcard");
    loop {
        match sdio.init(ClockFreq::F12Mhz) {
            Ok(_) => break,
            Err(e) => info!("waiting for sdio - {:?}", defmt::Debug2Format(&e)),
        }

        delay.delay_ms(1000u32);
    }

    let nblocks = sdio.card().map(|c| c.block_count());
    info!("sdcard detected: nbr of blocks: {:?}", nblocks);
}
